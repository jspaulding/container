#!/usr/bin/env bash
set -euxo pipefail

FQ_IMAGE_NAME="${CI_REGISTRY_IMAGE}/${IMAGE_NAME}"

c=$(buildah from alpine:latest)
buildah run $c apk add nmap lua --no-cache
buildah config --entrypoint '["/usr/bin/nmap"]' $c
buildah commit --squash $c $FQ_IMAGE_NAME
buildah push ${FQ_IMAGE_NAME}
